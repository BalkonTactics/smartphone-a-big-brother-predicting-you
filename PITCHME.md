### SMARTPHONE
PRIVACY CHECK UP

Big Brother predicting you

Coffe & circunvention Amsterdam Meetup
 bij @suysulucha and @balkontactics

#VSLIDE

### Functionalities and risk are trippled

* GSM network
* Smart device
* Personal laptop

#HSLIDE

### GSM network  aka localization!

 * Is obsoleted technology
 * Easy to intercept and eavesdrop, by administrators and third parties
 * Network composed by cells and antennas

<a href="https://www.researchgate.net/figure/An-example-of-mobile-phone-network-localization-data-types-for-one-user-travelling-from-a_fig1_274376732"><img src="https://www.researchgate.net/profile/Mathieu_Bredif/publication/274376732/figure/fig1/AS:294796158619650@1447296289622/An-example-of-mobile-phone-network-localization-data-types-for-one-user-travelling-from-a.png" alt="An example of mobile phone network localization data types for one user travelling from a point X to a point Y"/></a>
[example of mobile phone-network-localization by Mathieu_Bredif]



#VSLIDE

## triangulation

* Telecommunications works only if your device is permanently localized 
* localization based on you device pinging 3 antennas [triangulation]
* This technology defeats the little anonymity we could of had

[Image of cell pinging by L. Scott Harrel](https://www.google.com/imgres?imgurl=http%3A%2F%2Fpursuitmag.com%2Fwp-content%2Fuploads%2F2008%2F07%2Fcell-phones-300x225.jpg&imgrefurl=http%3A%2F%2Fpursuitmag.com%2Flocating-mobile-phones-through-pinging-and-triangulation%2F&docid=bIiCyFl-oJMWrM&tbnid=vN8ZVM749omc-M%3A&vet=10ahUKEwiK0fbggZLlAhUnwMQBHc_6CwcQMwhEKAAwAA..i&w=300&h=225&client=ubuntu&bih=607&biw=954&q=Image%20of%20cell%20pinging%20by%20L.%20Scott%20Harrell&ved=0ahUKEwiK0fbggZLlAhUnwMQBHc_6CwcQMwhEKAAwAA&iact=mrc&uact=8) [Image of cell pinging by L. Scott Harrel]

## Consequenncies

* **supplant** an operator/cell tower [IMSI CATCHERS] and therefore ...
* Access to information that allow to know **location** and **time stamps**
* Redirect a call to you, **record it** and send it to the legitimate destination
* Send **SMS (massively)** on behalf of another carrier


#VSLIDE

## If above wasnot enough... Data retention law


**Telephone service providers** and **internet service providers** are obligated to keep up to **1 year** all associated information related to the Telecommunications.

#VSLIDE

## Which data they keep?

[Metadata]

* IMEI - telephone locate number
* IMSI - SIM card locate number
* Sender and receiver from calls, sms
* Data conexxion: volume, duration,
* time stamps and geolocation [triangulation]

#VSLIDE

## Consequences from a third party having access/intercepting to this metadata are

* Able to predict your daily routing: where to find u
* Able to demonstrate presence: where u have been place+time
* Able to demonstrate with who you hang around: visualize your social graphs

#VSLIDE


## Case Malte Spitz on visualization of data retention law


2009: [Malte Spitz, eurodiputat verd](http://www.zeit.de/datenschutz/malte-spitz-data-retention)


#HSLIDE

# Smart device


#VSLIDE

## smart device - definition

 [definition](https://www.techopedia.com/definition/31463/smart-device)


#VSLIDE

## How many sensors has my smart phone?

 Lets discover some of the most fequent ones, and why they are used for:

 [Accelometer Gyroscope Magnetometer Proximity Ambient light sensor Pedometer Fingerprinting Sensor Touch screen Barometer  Heart sensor GPS  Microphone](https://fossbytes.com/which-smartphone-sensors-how-work/)
  
#VSLIDE


## Consequences from a third party having access/intercepting to this metadata are

* Able to predict your daily routing: where to find you
* Able to demonstrate presence: where you have been place + time
* Able to demonstrate with who you hang around: able to visualize your social graphs

#VSLIDE

## We install ANY app anyone suggested us

* In internet if you dont pay, your data becomes the product
* Apps tend to ask excesive permits to gather data from you

#HSLIDE

## Personal Laptop

* we need to think on WIFI, Bluetooh and NFC connextions
* We need to learn to DELETE data: fotos, mails, sms, diferent histories we store...
* 

#HSLIDE



## Operational vs Instrumental Security

OPSEC is the security that has to do with our behaviour and daily habits

INFOSEC is the security that has to do with technology and tools to democratize security

Knowing that security is not astatic and that has lots of limitations/definitions per situation& person
 we can agree to have 4 common standars when we are pursuing for security and privacy:
 
 * open source 
 * decentralization
 * security by default
 * usability
 * 

#HSLIDE

 
## HANDS ON

## Lets realize the problem here

1. Make a list of: [is not exhaustive]
      mesages how many and since when, which apps have acces to them
      photografies how many and since when, which apps have acces to them
      Mails how many, from how many accounts, since when, which apps have access to them
      Financial data: credit card, bank accounts, security codes, pins, paypal, financial apps...
      Passwords, how many, where do you store them, since when, who has access
      wifi history, how many wifis have you saved, since when ... does it define you?
      browing history, since when, how many browsers, does it define you?
      

## 2. Determining Sensitivity Levels for Shared Data

    from previous information, now determine the sensitivity it has for you
    
    RED list: passwords, readl full name & address: home, work or school, social security number, government ID, place and date of birth, biometric info, device IMSi and IMEI, specific locations
    YELLOW LIST: alias or digital names, primary email, telephone number, race, sexual orientation and gender, Mailing address
    GREEN LIST: secondary names, secondary emails, 

## 3. Lets start with LEARNING TO DELETE

## 4. Continue to review Apps permits

## 5. Review Googles settings

## 6. Install some privacy approach apps

  Signal to substitute whatsapp
  Orfox to csubtitute google chrome
    

 


